import { EnumURL, getTokenLocal } from "../utils/Common";
import HttpService from "./getWays/Setting.GetWay";
import UserDocument from "../interfaces/User.Interface";
import OrderDocument from "../interfaces/Order.Interface";
import {
  PaypalDocument,
  PaypalGuestInfo,
} from "../interfaces/Paypal.Interface";

//---User
export const Register = async (user: UserDocument) => {
  return await HttpService.post(EnumURL.user, {
    firstName: user.firstName,
    lastName: user.lastName,
    address: user.address,
    phone: user.phone,
    email: user.email,
    password: user.password,
    cPassword: user.cPassword,
  });
};

export const Login = async (email: string, password: string) => {
  return await HttpService.post(EnumURL.login, {
    email: email,
    password: password,
  });
};

export const GetProfile = async () => {
  return await HttpService.get(EnumURL.profile, {
    headers: {
      Authorization: "Bearer " + getTokenLocal(),
    },
  });
};

export const UpdateProfile = async (user: UserDocument) => {
  return await HttpService.put(EnumURL.profile, {
    firstName: user.firstName,
    lastName: user.lastName,
    address: user.address,
  });
};

export const ChangePassword = async (
  password: string | undefined,
  newPassword: string | undefined,
  cNewPassword: string | undefined
) => {
  return await HttpService.put(EnumURL.changePassword, {
    password: password,
    newPassword: newPassword,
    cNewPassword: cNewPassword,
  });
};

export const ForgotPassword = async (email: string) => {
  return await HttpService.post(EnumURL.forgot, {
    email: email,
  });
};

//---Product
export const GetProductByID = async (id: any) => {
  return await HttpService.get(`${EnumURL.product}/${id}`);
};

export const GetListProducts = async () => {
  return await HttpService.get(EnumURL.products);
};

export const GetListProductsByCategory = async (type: string | undefined) => {
  return await HttpService.get(`${EnumURL.products}/${type}`);
};

export const GetListProductByCategoryAndSortedByName = async (
  categoryID: string,
  type: string
) => {
  return await HttpService.post(
    `${EnumURL.sort.product.base}/name/category/${categoryID}`,
    {
      type: type,
    }
  );
};

export const GetListProductByCategoryAndSortedByPrice = async (
  categoryID: string,
  type: string
) => {
  return await HttpService.post(
    `${EnumURL.sort.product.base}/price/category/${categoryID}`,
    {
      type: type,
    }
  );
};

export const GetListProductSortedByNameAndPrice = async (
  typeName: string,
  typePrice: string
) => {
  return await HttpService.post(EnumURL.sort.product.base, {
    typeName: typeName,
    typePrice: typePrice,
  });
};

export const GetListProductByCategorySortedByNameAndPrice = async (
  categoryID: string,
  typeName: string,
  typePrice: string
) => {
  return await HttpService.post(`${EnumURL.sort.product.base}/${categoryID}`, {
    typeName: typeName,
    typePrice: typePrice,
  });
};

export const GetListProductSortedByName = async (type: string) => {
  return await HttpService.post(EnumURL.sort.product.name, {
    type: type,
  });
};

export const GetListProductSortedByPrice = async (type: string) => {
  return await HttpService.post(EnumURL.sort.product.price, {
    type: type,
  });
};

//---Category
export const GetListCategories = async () => {
  return await HttpService.get(EnumURL.categories);
};

export const DeleteProduct = async (id: string | undefined) => {
  return await HttpService.delete(`${EnumURL.product}/${id}`);
};

//---Cart
export const AddToCart = async (
  idProduct: string,
  quantity: number,
  size: string
) => {
  return await HttpService.post(EnumURL.cart, {
    product: idProduct,
    quantity: quantity,
    size: size,
  });
};

export const GetListCarts = async () => {
  return await HttpService.get(EnumURL.carts);
};

export const GetCartPriceSelected = async (list: string[] | undefined) => {
  return await HttpService.post(`${EnumURL.cart}/price`, {
    carts: list,
  });
};

export const GetCartQuantity = async () => {
  return await HttpService.get(`${EnumURL.carts}/quantity`);
};

export const UpdateCartByID = async (
  id: string,
  quantity: number,
  size: string
) => {
  return await HttpService.put(EnumURL.cart, {
    product: id,
    quantity: quantity,
    size: size,
  });
};

export const DeleteCartByID = async (id: string | undefined) => {
  return await HttpService.delete(`${EnumURL.cart}/${id}`);
};

export const DeleteListCart = async (list: string[] | undefined) => {
  return await HttpService.post(EnumURL.deleteListCarts, {
    carts: list,
  });
};

export const DeleteAllCart = async () => {
  return await HttpService.delete(`${EnumURL.cart}/all`);
};

//Order
export const CreateOrder = async (order: OrderDocument) => {
  return await HttpService.post(EnumURL.order, {
    comment: order.comment,
    infoGuest: order.infoGuest,
    orderDetails: order.orderDetails,
  });
};

export const GetListOrder = async () => {
  return await HttpService.get(EnumURL.order);
};

export const GetOrderHistory = async (status: string) => {
  return await HttpService.get(`${EnumURL.ordersTracking}/${status}`);
};

export const GetProductsForOrder = async (list: any[]) => {
  return await HttpService.post(`${EnumURL.products}/${EnumURL.order}`, {
    list: list,
  });
};

export const CancelOrder = async (id: string) => {
  return await HttpService.post(`${EnumURL.order}/cancel/${id}`);
}

//Statistic
export const StatisticOrdered = async () => {
  return await HttpService.get(EnumURL.statistic);
};

//Paypal
export const GetPricePaypalToCreate = async (items: PaypalDocument[]) => {
  return await HttpService.post(`${EnumURL.paypal}/price`, {
    list: items,
  });
};

export const SavePaypal = async (
  orderID: string,
  carts: PaypalDocument[],
  infoGuest: PaypalGuestInfo
) => {
  return await HttpService.post(EnumURL.paypal, {
    orderID: orderID,
    orderDetails: carts,
    infoGuest: infoGuest,
  });
};
