import React from 'react'

const HomeBanner = () => {
  return (
    <div className="home__banner">
      <img src="https://images.squarespace-cdn.com/content/v1/550c7d8fe4b00ccc1d9769a8/1516336239822-JYSI4JPA1RPKNX00FRZV/Product+-+banner.jpg?format=2500w" alt="banner"/>
    </div>
  )
}

export default HomeBanner
